from PyQt6.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QMessageBox
from models import User

class RegisterForm(QWidget):
    def __init__(self, main_app, session):
        super().__init__()
        self.main_app = main_app
        self.session = session
        
        layout = QVBoxLayout()
        
        name_label = QLabel("Name")
        self.name_input = QLineEdit()
        
        email_label = QLabel("Email")
        self.email_input = QLineEdit()
        
        password_label = QLabel("Password")
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.EchoMode.Password)
        
        register_button = QPushButton("Register")
        register_button.clicked.connect(self.register_user)
        
        layout.addWidget(name_label)
        layout.addWidget(self.name_input)
        layout.addWidget(email_label)
        layout.addWidget(self.email_input)
        layout.addWidget(password_label)
        layout.addWidget(self.password_input)
        layout.addWidget(register_button)
        
        self.setLayout(layout)
    
    def register_user(self):
        name = self.name_input.text()
        email = self.email_input.text()
        password = self.password_input.text()
        
        if not name or not email or not password:
            QMessageBox.warning(self, "Error", "Please fill in all fields.")
            return
        
        new_user = User(name=name, email=email, password=password)
        self.session.add(new_user)
        self.session.commit()
        
        QMessageBox.information(self, "Success", "Registration successful!")
        self.main_app.show_login()