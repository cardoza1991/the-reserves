from PyQt6.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QMessageBox
from models import User

class LoginForm(QWidget):
    def __init__(self, main_app, session):
        super().__init__()
        self.main_app = main_app
        self.session = session
        
        layout = QVBoxLayout()
        
        email_label = QLabel("Email")
        self.email_input = QLineEdit()
        
        password_label = QLabel("Password")
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.EchoMode.Password)
        
        login_button = QPushButton("Login")
        login_button.clicked.connect(self.login_user)
        
        layout.addWidget(email_label)
        layout.addWidget(self.email_input)
        layout.addWidget(password_label)
        layout.addWidget(self.password_input)
        layout.addWidget(login_button)
        
        self.setLayout(layout)
    
    def login_user(self):
        email = self.email_input.text()
        password = self.password_input.text()
        
        user = self.session.query(User).filter_by(email=email).first()
        
        if user and user.password == password:
            self.main_app.show_home_screen(user)
        else:
            QMessageBox.warning(self, "Error", "Invalid email or password.")
