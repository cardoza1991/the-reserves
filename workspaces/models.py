from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import declarative_base, relationship, sessionmaker

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String, unique=True)
    password = Column(String)  # Plain text for initial testing; consider hashing later

class Workspace(Base):
    __tablename__ = 'workspaces'
    id = Column(Integer, primary_key=True)
    location = Column(String)
    capacity = Column(Integer)

class Reservation(Base):
    __tablename__ = 'reservations'
    id = Column(Integer, primary_key=True)
    workspace_id = Column(Integer, ForeignKey('workspaces.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    start_time = Column(DateTime)
    end_time = Column(DateTime)

    workspace = relationship("Workspace")
    user = relationship("User")

def init_db():
    engine = create_engine('sqlite:///reservations.db')
    Base.metadata.create_all(engine)
    return sessionmaker(bind=engine)()

if __name__ == "__main__":
    session = init_db()
    # Example of creating a new user for testing
    new_user = User(name="Test User", email="test@example.com", password="test1234")
    session.add(new_user)
    session.commit()
    print("User added successfully.")
