from PyQt6.QtWidgets import QWidget, QVBoxLayout, QLabel, QListWidget, QPushButton
from models import Reservation

class ReservationsListScreen(QWidget):
    def __init__(self, main_app, user, session):
        super().__init__()
        self.main_app = main_app
        self.user = user
        self.session = session
        
        layout = QVBoxLayout()
        
        reservations_label = QLabel("Your Reservations")
        self.reservations_list = QListWidget()
        self.load_reservations()
        
        back_button = QPushButton("Back")
        back_button.clicked.connect(self.go_back)
        
        layout.addWidget(reservations_label)
        layout.addWidget(self.reservations_list)
        layout.addWidget(back_button)
        
        self.setLayout(layout)
    
    def load_reservations(self):
        reservations = self.session.query(Reservation).filter_by(user=self.user).all()
        for reservation in reservations:
            item_text = f"{reservation.workspace.location} - {reservation.date}"
            self.reservations_list.addItem(item_text)
    
    def go_back(self):
        self.main_app.show_home_screen(self.user)