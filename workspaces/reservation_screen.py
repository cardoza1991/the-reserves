from PyQt6.QtWidgets import QWidget, QVBoxLayout, QLabel, QComboBox, QCalendarWidget, QPushButton, QMessageBox
from models import Workspace, Reservation
from PyQt6.QtCore import QDate
from datetime import datetime  # Make sure to import datetime

class ReservationScreen(QWidget):
    def __init__(self, main_app, user, session):
        super().__init__()
        self.main_app = main_app
        self.user = user
        self.session = session
        
        layout = QVBoxLayout()
        
        workspace_label = QLabel("Select Workspace")
        self.workspace_combo = QComboBox()
        self.load_workspaces()
        
        date_label = QLabel("Select Date")
        self.calendar = QCalendarWidget()
        
        reserve_button = QPushButton("Make Reservation")
        reserve_button.clicked.connect(self.make_reservation)
        
        layout.addWidget(workspace_label)
        layout.addWidget(self.workspace_combo)
        layout.addWidget(date_label)
        layout.addWidget(self.calendar)
        layout.addWidget(reserve_button)
        
        self.setLayout(layout)
    
    def load_workspaces(self):
        workspaces = self.session.query(Workspace).all()
        for workspace in workspaces:
            self.workspace_combo.addItem(workspace.location, workspace)
    
    def make_reservation(self):
        selected_workspace = self.workspace_combo.currentData()
        selected_date = self.calendar.selectedDate().toPyDate()
        
        start_time = datetime.combine(selected_date, datetime.min.time())  # Example start time
        end_time = datetime.combine(selected_date, datetime.max.time())  # Example end time

        try:
            new_reservation = Reservation(
                workspace_id=selected_workspace.id,  # Pass the ID of the workspace
                user_id=self.user.id,  # Pass the ID of the user
                start_time=start_time,  # Assuming your model uses these fields
                end_time=end_time
            )
            self.session.add(new_reservation)
            self.session.commit()
            QMessageBox.information(self, "Success", "Reservation created successfully!")
            self.main_app.show_home_screen(self.user)
        except Exception as e:
            self.session.rollback()
            QMessageBox.warning(self, "Error", "Failed to make reservation: " + str(e))

