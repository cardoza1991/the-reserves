from PyQt6.QtWidgets import QApplication, QMainWindow, QStackedWidget
import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from welcome import WelcomeScreen
from login import LoginForm
from register import RegisterForm
from home_screen import HomeScreen
from reservation_screen import ReservationScreen
from reservations_list import ReservationsListScreen
import logging

# Configure logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

# Database setup
engine = create_engine('sqlite:///reservations.db')  # Adjust the URL as needed
Session = sessionmaker(bind=engine)
session = Session()

def init_db():
    from models import Base  # Import Base from your models.py
    Base.metadata.create_all(engine)  # Create tables

class MainApplication(QMainWindow):
    def __init__(self, session):
        super().__init__()
        self.setWindowTitle("The Reserves")
        self.stacked_widget = QStackedWidget()
        self.setCentralWidget(self.stacked_widget)
        self.session = session

        # Initialize screens
        self.welcome_screen = WelcomeScreen(self)
        self.login_form = LoginForm(self, session)
        self.register_form = RegisterForm(self, session)
        self.home_screen = None
        self.reservation_screen = None
        self.reservations_list_screen = None

        # Add screens to the stacked widget
        self.stacked_widget.addWidget(self.welcome_screen)
        self.stacked_widget.addWidget(self.login_form)
        self.stacked_widget.addWidget(self.register_form)

        # Set the welcome screen as the first screen
        self.stacked_widget.setCurrentWidget(self.welcome_screen)

    def show_home_screen(self, user):
        if not self.home_screen:
            self.home_screen = HomeScreen(self, user)
            self.stacked_widget.addWidget(self.home_screen)
        self.stacked_widget.setCurrentWidget(self.home_screen)

    def show_login(self):
        self.stacked_widget.setCurrentWidget(self.login_form)  # Show the login form

    def show_reservation_screen(self, user):
        if not self.reservation_screen:
            self.reservation_screen = ReservationScreen(self, user, self.session)
            self.stacked_widget.addWidget(self.reservation_screen)
        self.stacked_widget.setCurrentWidget(self.reservation_screen)

    def show_reservations_list(self, user):
        if not self.reservations_list_screen:
            self.reservations_list_screen = ReservationsListScreen(self, user, self.session)
            self.stacked_widget.addWidget(self.reservations_list_screen)
        self.stacked_widget.setCurrentWidget(self.reservations_list_screen)

def main():
    init_db()  # Initialize the database before creating the application
    app = QApplication(sys.argv)
    main_window = MainApplication(session)
    main_window.show()
    sys.exit(app.exec())

if __name__ == "__main__":
    main()
