from PyQt6.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton

class HomeScreen(QWidget):
    def __init__(self, main_app, user):
        super().__init__()
        self.main_app = main_app
        self.user = user
        
        layout = QVBoxLayout()
        
        welcome_label = QLabel(f"Welcome, {user.name}!")  # Personalized welcome message
        
        reservation_button = QPushButton("Make Reservation")
        reservation_button.clicked.connect(self.show_reservation_screen)  # Connects to show_reservation_screen
        
        view_reservations_button = QPushButton("View Reservations")
        view_reservations_button.clicked.connect(self.show_reservations_list)  # Should connect to the correctly named method
        
        logout_button = QPushButton("Logout")
        logout_button.clicked.connect(self.logout_user)  # Connects to logout_user
        
        layout.addWidget(welcome_label)
        layout.addWidget(reservation_button)
        layout.addWidget(view_reservations_button)
        layout.addWidget(logout_button)
        
        self.setLayout(layout)

    def show_reservation_screen(self):
        self.main_app.show_reservation_screen(self.user)  # Assumes MainApplication has this method

    def show_reservations_list(self):  # Corrected method name
        self.main_app.show_reservations_list(self.user)  # Assumes MainApplication has this method

    def logout_user(self):
        self.main_app.show_login()  # Assumes MainApplication has this method
